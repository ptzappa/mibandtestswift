//
//  ViewController.swift
//  BLE_Test_Swift
//
//  Created by paparotnick on 30.09.16.
//  Copyright © 2016 Zappa. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var manager:CBCentralManager!
    var peripheralDevice:CBPeripheral!

    override func viewDidLoad() {
        super.viewDidLoad()
        manager = CBCentralManager(delegate: self, queue:nil)
    }
    
//  MARK: CBCentralManagerDelegate
    
    func centralManagerDidUpdateState(central: CBCentralManager) {
        print("*centralManagerDidUpdateState")
        if (central.state == .PoweredOn){
            manager.scanForPeripheralsWithServices(nil, options: nil)
        }
    }

    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        print("*ConnectPeripheral")
//        peripheralDevice = peripheral
        peripheralDevice.discoverServices(nil);
        peripheralDevice.delegate = self;
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        print("*DiscoverPeripheral, try to connect")
        print("\(advertisementData)")
        central.stopScan()
        peripheralDevice = peripheral
//        peripheralDevice.discoverServices(nil);
//        peripheralDevice.delegate = self;
        
        manager.connectPeripheral(peripheral, options: nil)
    }
    
//  MARK: CBPeripheralDelegate
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?)
    {
        print("*peripherial services")
        if let servicePeripherals = peripheral.services as [CBService]!
        {
            for servicePeripheral in servicePeripherals
            {
                peripheral.discoverCharacteristics(nil, forService: servicePeripheral)
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        
        print("*didDiscoverCharacteristicsForService")
        
        if let charactericsArr = service.characteristics  as [CBCharacteristic]!
        {
            for cc in charactericsArr
            {
                print(cc.UUID.UUIDString)
                if cc.UUID.UUIDString == "2A06"{
                    var parameter = NSInteger(1)
                    let data = NSData(bytes: &parameter, length: 2)
                    
                    //let data: NSData = "02".dataUsingEncoding(NSUTF8StringEncoding)!
                    print("vibration")
                    peripheral.writeValue(data, forCharacteristic: cc, type: CBCharacteristicWriteType.WithoutResponse)
                }
                
                peripheral.setNotifyValue(true, forCharacteristic: cc)
                
                if cc.UUID.UUIDString == "FF0E"{
                    //output("Characteristic", data: cc)
                    var parameter = NSInteger(2)
                    let data = NSData(bytes: &parameter, length: 2)
                    
                    let string1 = NSString(data: data, encoding: NSUTF8StringEncoding)
                    print(string1)
                    
                    //let data: NSData = "2".dataUsingEncoding(NSUTF8StringEncoding)!
                    peripheral.writeValue(data, forCharacteristic: cc, type: CBCharacteristicWriteType.WithoutResponse)
                    //output("Characteristic", data: cc)
                } else if cc.UUID.UUIDString == "FF06" {
                    print("READING STEPS")
                    peripheral.readValueForCharacteristic(cc)
                } else if cc.UUID.UUIDString == "FF0C" {
                    print("READING BATTERY")
                    peripheral.readValueForCharacteristic(cc)
                } else if cc.UUID.UUIDString == "FF01" {
                    print("READING FF01")
                    peripheral.readValueForCharacteristic(cc)
                }
            }
            
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        
        print("*didUpdateValueForCharacteristic")
        
        //output("Data for "+characteristic.UUID.UUIDString, data: characteristic.value!)
        
        if(characteristic.UUID.UUIDString == "FF06") {
            let u16 = UnsafePointer<Int>(characteristic.value!.bytes).memory
            print("\(u16) steps")
        } else if(characteristic.UUID.UUIDString == "FF0C") {
            var u16 = UnsafePointer<Int32>(characteristic.value!.bytes).memory
            u16 =  u16 & 0xff
            /*
                0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1
             &  0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1
             -------------------------------
                0 0 0 0 0 0 0 0 0 1 0 1 0 1 0 1
             */
            print("\(u16) % charged")
        }
    }
    
//  MARK: Other
    
    func output(description: String, data: AnyObject){
        print("\(description): \(data)", terminator: "")
        // textField.text = textField.text + "\(description): \(data)\n"
    }
    
    

    
    func printData(data: NSData){
        let nsData: NSData = NSData(bytes: [0x00, 0x02, 0x0A] as [UInt8], length: 3)
        let buffer = UnsafeBufferPointer<UInt8>(start:UnsafePointer<UInt8>(nsData.bytes), count:nsData.length)
        print("nsData: \(nsData)")
        
        var intData = [Int]()
        intData.reserveCapacity(nsData.length)
        
        var stringData = String()
        
        for i in 0..<nsData.length {
            intData.append(Int(buffer[i]))
            stringData += String(buffer[i])
        }
        
        print("intData: \(intData)")
        print("stringData: \(stringData)")
    }
}


//  MARK: Comments


/*
Log:
 
*centralManagerDidUpdateState
*DiscoverPeripheral, try to connect
*ConnectPeripheral
*peripherial services
*didDiscoverCharacteristicsForService
FF01
READING FF01
FF02
FF03
FF04
FF05
FF06
READING STEPS
FF07
FF08
FF09
FF0A
FF0B
FF0C
READING BATTERY
FF0D
FF0E
Optional()
FF0F
FF10
FEC9
*didDiscoverCharacteristicsForService
FEDD
FEDE
FEDF
FED0
FED1
FED2
FED3
*didDiscoverCharacteristicsForService
FEC7
FEC8
FEC9
*didDiscoverCharacteristicsForService
2A06
vibration
*didUpdateValueForCharacteristic
*didUpdateValueForCharacteristic
412 steps
*didUpdateValueForCharacteristic
36 % charged
*didUpdateValueForCharacteristic
*didUpdateValueForCharacteristic
*didUpdateValueForCharacteristic
*didUpdateValueForCharacteristic
*didUpdateValueForCharacteristic
 
 
 */



